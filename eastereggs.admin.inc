<?php

/**
 * @file
 * Contains the administration pages for EasterEggs.
 *
 */

function eastereggs_settings_save($form, $form_state, $action = 'add', $eid = 0) {
  $form['eid'] = array(
    '#type' => 'hidden',
    '#value' => $eid,
  );
  $form['eastereggs_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 32,
    '#maxlength' => 80,
    '#required' => 1,
  );
  $form['eastereggs_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#description' => t('Enable or disable the easteregg?'),
  );
  $form['eastereggs_prevent_in_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent on administration pages and node edit'),
    '#description' => t('Do you want to prevent EasterEggs on admin pages?'),
  );
  $form['eastereggs_trigger'] = array(
    '#type' => 'textfield',
    '#title' => t('String Trigger'),
    '#description' => t('Set the string of the EasterEggs trigger'),
    '#size' => 30,
    '#maxlength' => 32,
    '#required' => 1,
  );
  $form['eastereggs_effect'] = array(
    '#title' => t('Effect'),
    '#description' => t('Where should the EasterEggs appear?'),
    '#type' => 'select',
    '#options' => array(
      'bottom_right' => t('Bottom right'),
      'bottom_left' => t('Bottom left'),
      'bottom_center' => t('Bottom center'),
      'top_right' => t('Top right'),
      'top_left' => t('Top left'),
      'top_center' => t('Top center'),
    ),
  );
  $form['eastereggs_image'] = array(
    '#type'   => "managed_file",
    '#title'  => t("Choose a image"),
    '#descripion' => t("Choose a image to apper after your trigger."),
    '#upload_location'    => "public://",
    "#upload_validators"  => array("file_validate_extensions" => array("gif png jpg jpeg")),
    '#required'=>TRUE,
  );

  if ($action == 'edit') {
    $easteregg = db_select('eastereggs')
      ->fields('eastereggs')
      ->condition('eid', $eid)
      ->execute()->fetchObject();

    $form['eastereggs_name']['#default_value'] = $easteregg->name;
    
    $easteregg->active != '0' && $form['eastereggs_active']['#attributes'] = array('checked' => 'checked');
    $form['eastereggs_prevent_in_admin']['#default_value'] = $easteregg->prevent_in_admin;
    $form['eastereggs_trigger']['#default_value'] = $easteregg->str_tgr;
    $form['eastereggs_effect']['#default_value'] = $easteregg->effect;
    $form['eastereggs_image']['#default_value'] = $easteregg->fid;
    
    $file = file_load((int) $easteregg->fid);

    if($file){
      $image_item = array(
        'style_name' => 'thumbnail',
        'path' => $file->uri,
      );

      $img = theme('image_style',$image_item);
      $form['eastereggs_image']['#field_prefix'] = "<div class='eggPreview'>".$img."</div>";
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array('eastereggs_settings_submit')
  );


  return $form;
}

function eastereggs_settings_submit($form, &$form_state) {

  form_state_values_clean($form_state);

  if(isset($form['eastereggs_image']['#default_value'])){
      if($form['eastereggs_image']['#default_value'] != $form_state['values']['eastereggs_image']){
      eastereggs_delete_image($form['eastereggs_image']['#default_value']);
    }
  }

  // Image Permanet Save
  $image = $form_state['values']['eastereggs_image'];
  $file = file_load($image);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  file_usage_add($file, 'eastereggs', 'module', 0);


  $eid = $form_state['values']['eid'];
  $fields = array(
    "name" => $form_state['values']['eastereggs_name'],
    "active" => $form_state['values']['eastereggs_active'],
    "str_tgr" => $form_state['values']['eastereggs_trigger'],
    "prevent_in_admin" => $form_state['values']['eastereggs_prevent_in_admin'],
    "effect" => $form_state['values']['eastereggs_effect'],
    "fid" => $form_state['values']['eastereggs_image'],
  );
  
  db_merge('eastereggs')
  ->key(array('eid' => $eid))
  ->fields($fields)
  ->execute();

  drupal_set_message(t('The configuration options have been saved.'));
  drupal_goto('admin/config/media/eastereggs');
}

function eastereggs_settings_list() {

  $order = isset($_GET['order']) ? $_GET['order'] : 'eid';
  $sort = isset($_GET['sort']) ? $_GET['sort'] : 'asc';

  $result = db_select('eastereggs')
    ->fields('eastereggs')
    ->orderBy($order, $sort)
    ->range(0,20)
    ->execute();

  $header = array();
  $header[] = array('data' => t('Name'), 'field' => 'name');
  $header[] = array('data' => t('Trigger'), 'field' => 'str_tgr');
  $header[] = array('data' => t('Active'), 'field' => 'active');
  $header[] = array('data' => t('Prevent in Admin'), 'field' => 'prevent_in_admin');
  $header[] = array('data' => t('Effect'), 'field' => 'effect');
  $header[] = array('data' => t('Operations'));

  $rows = array();
  $destination = drupal_get_destination();

  foreach ($result as $data) {
    $row = array();
    $row['data']['name'] = $data->name;
    $row['data']['str_tgr'] = $data->str_tgr;
    $row['data']['active'] = $data->active == "1" ? t("On") : t("Off");
    $row['data']['prevent_in_admin'] = $data->prevent_in_admin == "0" ? t("No") : t("Yes");
    $row['data']['effect'] = $data->effect;

    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => "admin/config/media/eastereggs/edit/" . $data->eid,
      'query' => $destination,
    );

    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => "admin/config/media/eastereggs/delete/" . $data->eid,
      'query' => $destination,
    );

    $row['data']['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );
    $rows[] = $row;
  }
  
  $build = array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('Don\'t exist any easteregg saved.'),
  );

  return theme('table',$build);
}

/**
 * Implements page callback de delete().
 */
function eastereggs_delete($eid) {
  $confirm_form = drupal_get_form('eastereggs_confirm_form', $eid);
  return render($confirm_form);
}

function eastereggs_confirm_form($form, &$form_state, $eid) {

  $form = array('#id' => 'eastereggs_confirm_form');

  $form['eid'] = array(
    '#type' => 'hidden',
    '#value' => $eid,
  );

  $form['group'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Are you sure wanna delete?'),
  );

  $form['group']['confirm'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
    '#submit' => array('eastereggs_confirm_form_submit'),
  );

  $form['group']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('eastereggs_cancel_form_submit'),
  );

  return $form;
}

function eastereggs_cancel_form_submit() {
  return false;
}

function eastereggs_confirm_form_submit($form, $form_state){

  $fid = eastereggs_file($form_state['values']['eid']);
  eastereggs_delete_image($fid['fid']);
  
  db_delete('eastereggs')
    ->condition('eid', $form_state['values']['eid'])
    ->execute();

  drupal_set_message(t('EasterEggs have been deleted.'));
  drupal_goto('/admin/config/media/eastereggs');
}

function eastereggs_file($eid){
  $query = db_select('eastereggs','e');
  $query->join('file_managed', 'f', 'e.fid = f.fid');
  $result = $query
    ->fields('e',array('fid'))
    ->condition('e.eid', $eid)
    ->execute()
    ->fetchAssoc();

  return $result;
}

function eastereggs_delete_image($fid){
  $file = file_load($fid);
  file_usage_delete($file, 'eastereggs', 'module', 0);
  file_delete($file);
}
