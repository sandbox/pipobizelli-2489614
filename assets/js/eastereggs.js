(function ($) {
	Drupal.behaviors.EasterEggs = {
    attach: function(context, settings){
      var self = this;
      $("body", context).once('eastereggs', function(){
        $.each(settings.easterObj, function(){
        	self.cheet(this);
        });
      });
    },
    cheet: function(obj){
    	var tgr = this.trigger(obj.str_tgr),
    			cb = this.callback(obj.effect, obj.img_url) || function(){};

    	!obj.prevent_in_admin && cheet(tgr, function(){ cb(); });
    },
    trigger: function(str){
    	var arr = [];
    	for(var i=0; i<str.length; i++){
    		arr.push(str[i]);
    	}
    	return arr.join(" ");
    },
    callback: function(effect, imgUrl){
    	var $el = $("<img/>").attr("src", imgUrl).css({position: "fixed", zIndex: 9999})
        , self = this
        , arrFx = effect.split("_");

      return function(){ self.effect($el, arrFx); }

      /*switch (effect){
    		case "bottom_right":
    			return function(){
    				$("body").css("overflow-x", "hidden");
    				$el.css({
    					bottom: 0,
    					right: -$(window).width()
    				});
    				$("body").append($el);
    				$el.load(function() { $el.css("right", -$el.width());});
    				$el.animate({right: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({right: -$el.width()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;

    		case "bottom_left":
    			return function(){
    				$("body").css("overflow-x", "hidden");
    				$el.css({
    					bottom: 0,
    					left: -$(window).width()
    				});
    				$("body").append($el);
    				$el.load(function() { $el.css("left", -$el.width());});
    				$el.animate({left: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({left: -$el.width()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;

    		case "bottom_center":
    			return function(){
    				$("body").css("overflow-y", "hidden");
    				$el.css({
    					bottom: -$(window).height(),
    					left: "50%"
    				});
    				$("body").append($el);
    				$el.css({
  						bottom: -$el.height(),
  						"margin-left": -$el.width()/2
  					});
    				$el.animate({bottom: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({bottom: -$el.height()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;

    		case "top_right":
    			return function(){
    				$("body").css("overflow-x", "hidden");
    				$el.css({
    					top: 0,
    					right: -$(window).width()
    				});
    				$("body").append($el);
    				$el.load(function() { $el.css("right", -$el.width());});
    				$el.animate({right: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({right: -$el.width()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;

    		case "top_left":
    			return function(){
    				$("body").css("overflow-x", "hidden");
    				$el.css({
    					top: 0,
    					left: -$(window).width()
    				});
    				$("body").append($el);
    				$el.load(function() { $el.css("left", -$el.width());});
    				$el.animate({left: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({left: -$el.width()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;

    		case "top_center":
    			return function(){
    				$("body").css("overflow-y", "hidden");
    				$el.css({
    					top: -$(window).height(),
    					left: "50%"
    				});
    				$("body").append($el);
    				$el.css({
  						top: -$el.height(),
  						"margin-left": -$el.width()/2
  					});
    				$el.animate({top: 0}, 500, function(){
    					setTimeout(function(){
    						$el.animate({top: -$el.height()}, 500, function(){
    							$el.remove();
    						})
    					}, 1000);
    				});
    			}
    		break;
    	}*/
    },
    effect: function($el, arrFx){
      var obj = {};

      obj[arrFx[0]] = 0;
      obj[arrFx[1]] = -$(window).width();
          
      $("body").css("overflow-x", "hidden");
      $el.css(obj);
      $("body").append($el);
      $el.load(function() { $el.css(arrFx[1], -$el.width());});
      obj[arrFx[1]] = 0;
      $el.animate(obj, 500, function(){
        obj[arrFx[1]] = -$el.width();
        setTimeout(function(){
          $el.animate(obj, 500, function(){
            $el.remove();
          })
        }, 1000);
      });
    }
  }
})(jQuery);